package com.example.sebastianmarinr.tecnowilly.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.sebastianmarinr.tecnowilly.Interface.IItemClickListener
import com.example.sebastianmarinr.tecnowilly.Model.Product
import com.example.sebastianmarinr.tecnowilly.R

class ProductAdapter(
    private var context: Context,
    private var productList: ArrayList<Product> ):RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private lateinit var product: Product
    lateinit var iItemClickListener: IItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.product_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        product = productList[position]
        Glide.with(context).load(productList[position].host + productList[position].image[0].path).into(holder.img_product)
        holder.txt_description.text=productList[position].description
        holder.txt_price.text= "$"+productList[position].price
        holder.txt_product.text = productList[position].categoryname.name
        holder.bindProduct(product)
    }


    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView),View.OnClickListener{

        private var product: Product? = null

        internal var img_product: ImageView = itemView.findViewById(R.id.product_image) as ImageView
        internal var txt_description: TextView = itemView.findViewById(R.id.product_description) as TextView
        internal var txt_price : TextView = itemView.findViewById(R.id.product_price) as TextView
        internal var txt_product: TextView = itemView.findViewById(R.id.product_name) as TextView

        fun bindProduct(product: Product) {
            this.product = product
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            iItemClickListener.itemSelected(this.product!!)
        }
    }
}