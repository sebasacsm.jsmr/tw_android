package com.example.sebastianmarinr.tecnowilly.Retrofit

import com.example.sebastianmarinr.tecnowilly.Model.ProductModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST

interface IProductList {
        @GET("indexApi")
        fun getProduct(): Observable<ProductModel>
}