package com.example.sebastianmarinr.tecnowilly.Activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.sebastianmarinr.tecnowilly.R
import kotlinx.android.synthetic.main.activity_contact.*


class ContactActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

            //boton clic para obtener entrada y llamar al método SendMail
        btnSendEmail.setOnClickListener {

            //obtener entrada de EditTexts y guardar en variable
        val body = messageEt.text.toString().trim() + " \n" +
                "Mi nombre es: " + UserNameEt.text.toString().trim() + ", " +
                "Numero Telefonico: " + UserPhoneEt.text.toString().trim() + ", " +
                "Correo Electronico: " + UserEmailEt.text.toString().trim()

            //method call for email intent with these inputs as parameters
            SendEmail(body)
        }

    }

    private fun SendEmail(body: String) {

        val mIntent = Intent(Intent.ACTION_SEND)
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("w.servicios@hotmail.com"))
        mIntent.putExtra(Intent.EXTRA_TEXT, body)
        mIntent.putExtra(Intent.EXTRA_SUBJECT, "Solicitud Información/Servicio ")

        //se crea excepcion para validar si tiene o no cliente de correo instalado
        try {
            startActivity(Intent.createChooser(mIntent,"No tiene cliente de correo Instalado..."))

        }
        catch (e: Exception){
            //si no cuenta con un cliente de correo
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

}


