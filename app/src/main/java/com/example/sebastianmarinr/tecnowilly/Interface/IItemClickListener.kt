package com.example.sebastianmarinr.tecnowilly.Interface


import com.example.sebastianmarinr.tecnowilly.Model.Product

interface IItemClickListener{
    fun itemSelected(product: Product)

}
