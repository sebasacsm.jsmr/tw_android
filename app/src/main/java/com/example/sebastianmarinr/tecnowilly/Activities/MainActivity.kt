package com.example.sebastianmarinr.tecnowilly.Activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import com.example.sebastianmarinr.tecnowilly.Adapter.FragmentAdapter
import com.example.sebastianmarinr.tecnowilly.R

class MainActivity : AppCompatActivity() {

    //private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
private lateinit var viewPaperTabs: ViewPager
private lateinit var tabLayoutContainer: TabLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // se crea la accion para llamar el activity desde el boton flotante de contacto
        val boton = findViewById<FloatingActionButton>(R.id.btncontact)
        boton.setOnClickListener {
            val intent = Intent(this, ContactActivity::class.java)
            startActivity(intent)
        }
        tabLayoutContainer= findViewById(R.id.tab_layout_container)

        val fragmentAdapter= FragmentAdapter(supportFragmentManager)
        viewPaperTabs= findViewById(R.id.view_paper_tabs)
        viewPaperTabs.adapter = fragmentAdapter

        tabLayoutContainer.setupWithViewPager(viewPaperTabs)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_principal, menu)
        return true
    }
}
