package com.example.sebastianmarinr.tecnowilly.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.sebastianmarinr.tecnowilly.Activities.FragmentAbout
import com.example.sebastianmarinr.tecnowilly.Activities.FragmentProduct
import com.example.sebastianmarinr.tecnowilly.Activities.FragmentProfile


class FragmentAdapter (fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                FragmentAbout()
            }
            1 -> FragmentProduct()
            else -> {
                return FragmentProfile()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "HOME"
            1 -> "CATALOGO"
            else -> {
                return "PERFIL"
            }
        }
    }
}