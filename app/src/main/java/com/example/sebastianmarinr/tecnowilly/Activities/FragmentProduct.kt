package com.example.sebastianmarinr.tecnowilly.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sebastianmarinr.tecnowilly.Adapter.ProductAdapter
import com.example.sebastianmarinr.tecnowilly.Interface.IItemClickListener
import com.example.sebastianmarinr.tecnowilly.Model.Image
import com.example.sebastianmarinr.tecnowilly.Model.Product
import com.example.sebastianmarinr.tecnowilly.Model.ProductModel
import com.example.sebastianmarinr.tecnowilly.R
import com.example.sebastianmarinr.tecnowilly.Retrofit.IProductList
import com.example.sebastianmarinr.tecnowilly.Retrofit.RetrofitClient
import com.mancj.materialsearchbar.MaterialSearchBar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FragmentProduct : Fragment(), IItemClickListener {

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var productModel: ProductModel
    private var productAdapter: ProductAdapter? = null
    private lateinit var recyclerViewProduct: RecyclerView
    private lateinit var searchAdapter: ProductAdapter
    internal var lastSuggest: MutableList<String> = ArrayList()
    internal lateinit var searchBar: MaterialSearchBar


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewInflater = inflater.inflate(R.layout.fragment_product_list, container, false)
        recyclerViewProduct = viewInflater.findViewById(R.id.product_recycler_view)

        //Setup Search Bar
        searchBar = viewInflater.findViewById(R.id.searchBar)
        searchBar.setHint("Nombre del Producto")
        searchBar.setMaxSuggestionCount(0)

        /*
        // Si se desea mostrar los Sugeridos para la busqueda, activar lo contenido en este comentario.
        searchBar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val suggest = ArrayList<String>()
                for (search in lastSuggest)
                    if (search.toLowerCase().contains(searchBar.text.toLowerCase()))
                        suggest.add(search)
                searchBar.lastSuggestions = suggest
            }
        }) */

        searchBar.setOnSearchActionListener(object : MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {
            }

            override fun onSearchStateChanged(enabled: Boolean) {
                if (!enabled)
                    recyclerViewProduct.adapter = productAdapter
            }

            override fun onSearchConfirmed(text: CharSequence?) {
                startSearch(text.toString())
            }

        })
        callServiceProduct()

        return viewInflater
    }


    private fun startSearch(text: String) {
        if (productModel.products.isNotEmpty()) {
            val result = ArrayList<Product>()
            for (product in productModel.products)
                if (product.categoryname.name.toLowerCase().contains(text.toLowerCase()))
                    result.add(product)
            searchAdapter = ProductAdapter(activity!!, result)
            searchAdapter.iItemClickListener= this
            recyclerViewProduct.adapter = searchAdapter
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            println("Error")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener

    @SuppressLint("CheckResult")
    private fun callServiceProduct() {

        RetrofitClient.getRetrofitInstance().create(IProductList::class.java).getProduct()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                productAdapter = ProductAdapter(activity!!, it.products as ArrayList<Product>)
                productAdapter!!.iItemClickListener = this
                recyclerViewProduct.adapter = productAdapter
                recyclerViewProduct.setHasFixedSize(true)
                recyclerViewProduct.layoutManager = GridLayoutManager(activity!!, 1)


                //Search Bar
                //se crea una variable global para acceder al modelo sin modificar las propiedades
                this.productModel = it

                /* Si se desa mostrar en la barra todos los productos como recomendados habilitar las lineas de lastSugget*/
                /*lastSuggest.clear()
                for (product in productModel.products)
                    lastSuggest.add(product.categoryname.name) */

                searchBar.visibility = View.VISIBLE



            }, {
                println(it)
            }
            )
    }

    override fun itemSelected(product: Product) {

        activity?.let {
            val intent = Intent(it, ProductSelectedActivity::class.java)
            intent.putExtra("key_product", product)
            it.startActivity(intent)
        }
    }
}