package com.example.sebastianmarinr.tecnowilly.Activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.sebastianmarinr.tecnowilly.Model.Product
import com.example.sebastianmarinr.tecnowilly.R

class ProductSelectedActivity : AppCompatActivity() {

    //Inicializacion de las variables para el adaptador de ProductDetail
    private lateinit var product: Product
    private lateinit var product_image_all: ImageView
    private lateinit var product_name: TextView
    private lateinit var product_description: TextView
    private lateinit var product_description_long: TextView
    private lateinit var product_price: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_selected)

        if (intent.hasExtra("key_product")) {
            product = intent.getSerializableExtra("key_product") as Product
        }

        product_image_all = findViewById(R.id.product_image_all)
        product_name = findViewById(R.id.product_name)
        product_description = findViewById(R.id.product_description)
        product_description_long = findViewById(R.id.product_description_long)
        product_price = findViewById(R.id.product_price2)

        //se llenan los datos a cada unos de los campos
        Glide.with(this).load(product.host + product.image[0].path).into(this.product_image_all)
        product_price.text = "$"+product.price
        product_name.text = product.categoryname.name
        product_description.text = product.description
        product_description_long.text = product.characteristic
    }
}
