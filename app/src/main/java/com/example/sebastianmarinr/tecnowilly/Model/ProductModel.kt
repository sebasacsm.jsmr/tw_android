package com.example.sebastianmarinr.tecnowilly.Model

import java.io.Serializable

data class ProductModel(
    val products: List<Product>
)

data class Product(
    val category_id: Int,
    val categoryname: Categoryname,
    val characteristic: String,
    val description: String,
    val host: String,
    val id: Int,
    val image: List<Image>,
    val price: String
): Serializable

data class Categoryname(
    val created_at: String,
    val description: String,
    val id: Int,
    val name: String,
    val updated_at: String,
    val user_id: Int
): Serializable

data class Image(
    val id: Int,
    val path: String,
    val product_id: Int
): Serializable
